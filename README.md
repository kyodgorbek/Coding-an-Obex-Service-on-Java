# Coding-an-Obex-Service-on-Java
Coding an Obex Service on Java
import java.io;
import javax.microedition.midlet.*;
import javax.microedition.io*;
import javax.microedition.lcdui.*;
import javax.obex.*;

import java.io.IOException;

public final class DateClient implements Runnable {
         private Thread mClientThread = null;
	 private static DataClient inst = new DateClient();
	 private DateClient() {
	 }
	 public static DateClient() {
		 return inst;
	 }
	 private boolean mEndNow = false;
	 private String mPref = null;
	 private String mHeight = null;
	 private String mContact = null;
	 
	 private static final String url =
	 "irdaobex://discover;ias=DatingService";
	 public void setMyInfo(String inPref, String inHeight,
	 String inContact) {
		 mPref =inPref;
		 mHeight = inHeight;
		 mContact = inContact;
	 }
	 public void StartClient(){
	   if (mClientThread != null)
		   return;
	   mEndNow = false;
	   
	   // Start receive thread
	   mClientThread = new Thread(this);
	   mClientThread.start();
   }
   public void stopClient() {
	mEndNow = true;
	 try {
	      // Only on CLDC 1.1
	     // mClientThread.interrupt();
	     mClientThread.join();
	 } catch ( Exception ex) {
		 System.out.prinln("in stop client");
		 ex.printStackTrace();
         }
	 mClientThread = null;
	 
 }
 
 public void run() {
	 DataOutputStr
	 eam dos = null;
	 Operation op = null;
	 ClientSession ses = null;
	 int code = 0;
	 HeaderSet rep = null;
	 HeaderSet hdrs = null;
	 
	 while( !mEndNow) {
	  ses = null;
	  dos = null;
	  op = null;
	  try  {
	       ses (ClientSession) Connector.open(url);
	  } catch (IOException ex) {
		  // Discovery fails, sleep for a while an try again
		  try {
			Thread.sleep(30001);
		  } catch (Exception e){}
		continue;
	}
	try {
	    resp = ses.connect(null);
	    code = resp.getResponseCode();
	
	    if (code != ResponseCodes.OBEX_HTTP_OK) {
		throw new IOException("OBEX connect operation failed");
	    }
	    hdrs = ses.createHeaderSet();
	    op = ses.path(hdrs);
	    dos = null;
	    dos = op.openDataOutPutStream();
	    
	    if(dos != null) {
		dos.writeUTF(mPref);
		dos.writeUTF(mHeight);
		dos.writeUTF(mContact);
		dos.flush();
		dos.close();
		    
		code = op.getResponseCode();
		// System.output.println("before os close");
		if(code != ResponseCodes.OBEX_HTTP_OK) {
		     throw new IOException("OBEX failure after put operations");
		}
		// Wait until acceptor thread is done 
		try {
		       mServer.join();
		} catch (InterruptedException e) {} // Ignore
		try {
		      mClient.stopClient();
		} catch (Exception e) {} // Ignore
		
	}
	
	protected void pauseApp() {
	
	}
	
	protected void startApp() {
	   if (mForm == null) {
		mForm = new Form("OBEXMIDlet");
		mDateContact = new StringItem("Potential date found at:" ,null);
		mForm.append(mDateContact);
		mForm.setCommandListener(this);
	   }
	   mDisplay = Display.getDisplay(this);
	   mDisplay.setCurrent(mForm);
	   mEndNow = false;
	   startServer();
	   mClient.setMyInfo(myPref, myHeight, myContact);
	   mClient.startClient();
	   mOpHandler = new OperationHandler(this);
   }
   private void startServer() {
	   if (mServer != null)
		return;
	   // Start server thread
	   mServer = new Thread(this);
	   mServer.start();
	   
   }
   
   public void run() {
	   try {
		mServerNotifier = (SessionNotifier) Connector.open(url);
	   } Catch (Exception e) {
		System.err.println("Can't initialize OBEX server:" + e);
	   }
	   
	    while(!mEndNow) {
		    mConnection = null;
		    
		    try {
			 mConnection = mServerNotifier.acceptAndOpen(mOpHandler);
		    } catch (IOException e){
			    continue;
		    }
		    
		//      System.out.println(" got a connection!");
                try {
                      // Bad API design, need to synchronize server thread
                       synchronized(this) {
                           this.wait();
		       }
     
                    //            System.outprintln("svr: before conn close");
		    mConnection.close();
	       } catch (Exception ex) {
		       // Log exception
	       }
	
       } // of while
       try {
	     mServerNotifier.close();
       } catch (Exception ex) {
	       System.out.println("trying to close session...exception");
	       ex.printStackTrace();
	
       }
}

private void processRequest(DataInputStream dis) {
	String pref = null;
	String height = null;
	String contact = null;
	try {
	       pref = dis.readUTF();
		height = dis.readUTF( );
		contact = dis.readUTF();
		
		                dis.close();
	} catch (IOException e) {
		System.out.prinln("in process request exception");
		e.printStackTrace();
	}
	if(! mLasrContact.equals(contact)) {
	    mLastContact = contact;
	    if(pref.equals)(seekPref)  && height.equals(seekHeight))
	       mDisplay.callSerially(new ShowCandidate(contact);
    }
}

class OperationHandler extends ServerRequastHandler {
	DataInputStream dis = null;
	Object synObject = null;
	public OperationHandler( Object inSync) {
		syncObject = inSync;
	}
	public int onPut(Operation op) {
	    dis = null;
	    try {
		dis = null;
		try {
		     dis = op.openDataInputStream();
		} catch (Exception ex) {
			// Okay for CREATE_EMPTY op
		}
		if(dis != null)     // Not a CREATE_EMPTY op
		{
		   processRequest(dis);
		    try {
			 dis.close();
			 op.close();
		    } catch (Exception ex) {
		    }
		    dis = null;
		    synchronized(syncObject) {
			    syncObject.notify();
		    }
	    }
	    return ResponseCodes:OBX_HTTP_OK;
    }
}
     class ShowCandidate implements Runnable {
	  Display disp = null;
	  String contact = null;
	  public ShowCandidate(String cont) {
		  contact = cont;
	  }
	  public void run() {
		  mDateContact.setText(contact );
	  }
    }
  
}
  
		  
		
